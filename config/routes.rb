Rails.application.routes.draw do
	root 'about#welcome'
  devise_for :users, controllers: { registrations: 'registrations' }

  resources :posts do
  	collection do
  		get :all, action: :index, defaults: { all: true }
  	end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
